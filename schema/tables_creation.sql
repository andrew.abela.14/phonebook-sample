create table if not exists phonebookdb.company(
	id int unsigned auto_increment,
	name varchar(255) not null,
	registration_date datetime not null,
	primary key (id),
    unique (name)
);

create table if not exists phonebookdb.person(
	id int unsigned auto_increment,
    last_name varchar(255) not null,
    first_name varchar(255) not null,
    address_line_1 varchar(255) null,
    address_line_2 varchar(255) null,
    town varchar(255) not null,
    company_id int unsigned not null,
    primary key (id),
    foreign key (company_id) references company(id)
);