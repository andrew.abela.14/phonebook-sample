using PhoneBook.Applicaton;
using PhoneBook.DataAccess;
using PhoneBookAPI.Behaviours;

var builder = WebApplication.CreateBuilder(args);

builder.WebHost.ConfigureServices((hostContext, services) =>
{

    // Add services to the container.
    services.AddApplicationServices();
    services.AddDataAccess(hostContext.Configuration);

    services.AddControllers();
    // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();

});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ValidationExceptionHandlerMiddleware>();
app.UseAuthorization();

app.MapControllers();

app.Run();
