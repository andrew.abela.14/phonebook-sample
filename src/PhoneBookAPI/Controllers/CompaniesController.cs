﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Handlers;
using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CompaniesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CompaniesController(IMediator mediator) 
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type=typeof(IReadOnlyCollection<CompanyDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _mediator.Send(new GetAllCompaniesRequest());
            return Ok(result);
        }

        [HttpGet("Company")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(CompanyDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByIdAsync([FromQuery][Required]uint companyId)
        {
            var result = await _mediator.Send(new GetCompanyRequest(companyId));
            return Ok(result);
        }

        [HttpPost("Company")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddNewCompanyAsync([FromBody]NewCompnayDto newCompnay)
        {
            await _mediator.Send(new NewCompanyRequest(newCompnay));
            return Ok();
        }
    }
}
