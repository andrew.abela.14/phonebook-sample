﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Handlers;
using System.ComponentModel.DataAnnotations;

namespace PhoneBookAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PeopleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PeopleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IReadOnlyCollection<PersonDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetAllAsync()
        {
            var result = await _mediator.Send(new GetAllPeopleRequest());
            return Ok(result);
        }

        [HttpGet("Search")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IReadOnlyCollection<PersonDto>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> SearchAsync(
            [FromQuery]string? lastName,
            [FromQuery] string? firstName,
            [FromQuery] string? addressLine1,
            [FromQuery] string? addressLine2,
            [FromQuery] string? town,
            [FromQuery] string? companyName)
        {
            var result = await _mediator.Send(new SearchPeopleRequest(lastName, firstName, addressLine1, addressLine2, town, companyName));
            return Ok(result);
        }

        [HttpGet("Person")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PersonDto))]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetByIdAsync([FromQuery][Required] string personId)
        {
            var result = await _mediator.Send(new GetPersonRequest(personId));
            return Ok(result);
        }

        [HttpPost("Person")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> AddNewPersonAsync([FromBody]PersonDto newPerson)
        {
            await _mediator.Send(new NewPersonRequest(newPerson));
            return Ok();
        }

        [HttpPatch("Person")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> UpdatePersonAsync([FromBody] UpdatePersonDto updatedPerson)
        {
            await _mediator.Send(new UpdatePersonRequest(updatedPerson));
            return Ok();
        }

        [HttpDelete("Person")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> DeletePersonAsync([FromQuery]uint personId)
        {
            await _mediator.Send(new DeletePersonRequest(personId));
            return Ok();
        }
    }
}
