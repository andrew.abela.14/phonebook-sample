using AutoFixture;
using AutoMapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Moq;
using PhoneBook.Applicaton;
using PhoneBook.Applicaton.Convertors;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Handlers;
using PhoneBook.DataAccess;
using PhoneBook.DataAccess.Models;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.UnitTests
{
    public class Tests
    {
        private readonly Fixture fixture = new();

        [Fact]
        public async Task Company_Add()
        {
            var dbContext = GetInMemoryDbContext();
            var companyRepo = new CompanyRepository(dbContext);
            var handler = new NewCompanyHandler(companyRepo, GetAutoMapper());
            var request = fixture.Build<NewCompanyRequest>().Create();

            var result = await handler.Handle(request, new CancellationToken());

            result.Should().BeTrue();
        }

        [Fact]
        public async Task Company_GetAll()
        {
            var dbContext = GetInMemoryDbContext();
            var companyRepo = new CompanyRepository(dbContext);
            var personRepo = new PersonRepository(dbContext);
            var handler = new GetAllCompaniesHandler(companyRepo, personRepo);
            var request = new GetAllCompaniesRequest();
            var companies = await RandomFillTables(dbContext);

            var expectedCompanies = companies.Select(c => new CompanyDto
            {
                Name = c.Name,
                RegistrationDate = c.RegistrationDate,
                NoPeople = c.People.Count
            });
            var result = await handler.Handle(request, new CancellationToken());

            result.Should().BeEquivalentTo(expectedCompanies);
        }

        [Fact]
        public async Task Person_Add()
        {
            var dbContext = GetInMemoryDbContext();
            var companies = await RandomFillTables(dbContext);

            await Test_AddNewPerson(
                dbContext,
                fixture.Build<PersonDto>()
                .With(p => p.CompanyName, companies.ElementAt(0).Name)
                .Create());
        }

        [Fact]
        public async Task Person_Add_Edit_Remove()
        {
            var dbContext = GetInMemoryDbContext();
            var companyRepo = new CompanyRepository(dbContext);
            var personRepo = new PersonRepository(dbContext);
            var companies = await RandomFillTables(dbContext);

            var newPerson = fixture.Build<PersonDto>()
                .With(p => p.CompanyName, companies.ElementAt(0).Name)
                .Create();

            //add
            await Test_AddNewPerson(dbContext, newPerson);
;
            //edit
            var currentPerson = await dbContext.People
                .Where(p => newPerson.LastName == p.LastName && newPerson.FirstName == p.FirstName &&
                            newPerson.AddressLine1 == p.AddressLine1 && newPerson.AddressLine2 == p.AddressLine2 &&
                            newPerson.Town == p.Town)
                .Include(p => p.Company)
                .FirstAsync(p => newPerson.CompanyName == p.Company.Name);

            var updatedPerson = fixture.Build<UpdatePersonDto>()
                .With(p => p.Id, currentPerson.Id)
                .With(p => p.CompanyName, companies.ElementAt(companies.Count - 1).Name)
                .Create();

            var updateHandler = new UpdatePersonHandler(personRepo, companyRepo, GetAutoMapper());
            var updateResult = await updateHandler.Handle(new UpdatePersonRequest(updatedPerson), new CancellationToken());

            updateResult.Should().BeTrue();

            //delete
            var deleteHandler = new DeletePersonHandler(personRepo);
            var deleteResult = await deleteHandler.Handle(new DeletePersonRequest(updatedPerson.Id), new CancellationToken());

            deleteResult.Should().BeTrue();
        }

        [Fact]
        public async Task Person_GetAll()
        {
            var dbContext = GetInMemoryDbContext();
            var personRepo = new PersonRepository(dbContext);
            var companies = await RandomFillTables(dbContext);

            var handler = new GetAllPeopleHandler(personRepo, GetAutoMapper());
            var request = new GetAllPeopleRequest();

            var result = await handler.Handle(request, new CancellationToken());

            var expectedPeople = ToPeopleDtos(companies);
            result.Should().BeEquivalentTo(expectedPeople);
        }

        [Fact]
        public async Task Person_Search()
        {
            var dbContext = GetInMemoryDbContext();
            var personRepo = new PersonRepository(dbContext);
            var companies = await RandomFillTables(dbContext);

            var handler = new SearchPeopleHandler(personRepo, GetAutoMapper());

            var compnayName = companies.ElementAt(0).Name;
            var request = new SearchPeopleRequest(CompanyName: compnayName);

            var expectedPeople = ToPeopleDtos(companies.ElementAt(0));
            var result = await handler.Handle(request, new CancellationToken());
            result.Should().BeEquivalentTo(expectedPeople);
        }

        [Fact]
        public async Task Person_WildCard()
        {
            var dbContext = GetInMemoryDbContext();
            var personRepo = new PersonRepository(dbContext);
            var companies = await RandomFillTables(dbContext);

            var totalPeople = companies.Sum(c => c.People.Count);
            Random rand = new();
            var randTestIndex = rand.Next(totalPeople); 

            Mock<IRandomNumberGenerator> mockRand = new();
            mockRand.Setup(s => s.Next(It.IsAny<int>())).Returns(randTestIndex);

            var handler = new GetPersonHandler(personRepo, mockRand.Object, GetAutoMapper());
            var request = new GetPersonRequest("*");

            var result = await handler.Handle(request, new CancellationToken());
            var expectedPeople = ToPeopleDtos(companies);
            result.Should().BeEquivalentTo(expectedPeople.ElementAt(randTestIndex));
        }

        private static async Task Test_AddNewPerson(PhoneBookDbContext dbContext, PersonDto newPerson)
        {
            var companyRepo = new CompanyRepository(dbContext);
            var personRepo = new PersonRepository(dbContext);
            var handler = new NewPersonHandler(personRepo, companyRepo, GetAutoMapper());

            var result = await handler.Handle(new NewPersonRequest(newPerson), new CancellationToken());

            result.Should().BeTrue();
        }

        private static IEnumerable<PersonDto> ToPeopleDtos(IEnumerable<Company> companies)
            => companies.SelectMany(ToPeopleDtos);

        private static IEnumerable<PersonDto> ToPeopleDtos(Company company)
            => company.People.Select(p => new PersonDto
            {
                LastName = p.LastName,
                FirstName = p.FirstName,
                AddressLine1 = p.AddressLine1,
                AddressLine2 = p.AddressLine2,
                Town = p.Town,
                CompanyName = company.Name
            });

        private static PhoneBookDbContext GetInMemoryDbContext()
        {
            var builder = new DbContextOptionsBuilder<PhoneBookDbContext>();
            builder.UseInMemoryDatabase(databaseName: "PhoneBookDbInMemory");

            var dbContextOptions = builder.Options;
            PhoneBookDbContext phoneBookDbContext = new(dbContextOptions);
            // Delete existing db before creating a new one
            phoneBookDbContext.Database.EnsureDeleted();
            phoneBookDbContext.Database.EnsureCreated();

            return phoneBookDbContext;
        }

        private async Task<IReadOnlyCollection<Company>> RandomFillTables(PhoneBookDbContext dbContext)
        {
            var rand = new Random();

            var companies = fixture.Build<Company>()
                .With(c => c.People, Array.Empty<Person>())
                .CreateMany(rand.Next(1, 10));

            foreach (var company in companies)
            {
                var people = fixture.Build<Person>()
                    .With(p => p.CompanyId, company.Id)
                    .With(p => p.Company, company)
                    .CreateMany(rand.Next(0, 10));

                company.People = people.ToList();
            }

            await dbContext.Companies.AddRangeAsync(companies);
            await dbContext.SaveChangesAsync();

            return companies.ToList();
        }

        private static IMapper GetAutoMapper()
        {
            var config = new MapperConfiguration(opts =>
            {
                opts.AddProfile(new AutoMappingProfile());
            });

            return config.CreateMapper();
        }
    }
}