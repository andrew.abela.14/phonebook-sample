﻿using Newtonsoft.Json;

namespace PhoneBook.Applicaton.Dtos
{
    public class PersonDto
    {
        [JsonProperty("lastName")]
        public string LastName { get; set; } = null!;

        [JsonProperty("firstName")]
        public string FirstName { get; set; } = null!;

        [JsonProperty("addressLine1")]
        public string? AddressLine1 { get; set; }

        [JsonProperty("addressLine2")]
        public string? AddressLine2 { get; set; }

        [JsonProperty("town")]
        public string Town { get; set; } = null!;

        [JsonProperty("companyName")]
        public string CompanyName { get; set; } = null!;
    }
}
