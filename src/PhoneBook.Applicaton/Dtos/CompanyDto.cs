﻿using Newtonsoft.Json;

namespace PhoneBook.Applicaton.Dtos
{
    public class CompanyDto : NewCompnayDto
    {
        [JsonProperty("noPeople")]
        public int NoPeople { get; set; }
    }
}
