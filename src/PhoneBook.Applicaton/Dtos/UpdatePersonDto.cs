﻿using Newtonsoft.Json;

namespace PhoneBook.Applicaton.Dtos
{
    public class UpdatePersonDto : PersonDto
    {
        [JsonProperty("id")]
        public uint Id { get; set; }
    }
}
