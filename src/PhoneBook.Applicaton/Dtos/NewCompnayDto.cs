﻿using Newtonsoft.Json;

namespace PhoneBook.Applicaton.Dtos
{
    public class NewCompnayDto
    {
        [JsonProperty("name")]
        public string Name { get; set; } = string.Empty;

        [JsonProperty("registrationDate")]
        public DateTime RegistrationDate { get; set; }
    }
}
