﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record GetCompanyRequest(uint Id) : IRequest<CompanyDto>;
    public class GetCompanyHandler : IRequestHandler<GetCompanyRequest, CompanyDto>
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public GetCompanyHandler(ICompanyRepository companyRepository, IMapper mapper)
        {
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public async Task<CompanyDto> Handle(GetCompanyRequest request, CancellationToken cancellationToken)
        {
            var result = await _companyRepository.GetByIdAsync(request.Id, cancellationToken);

            return result == null
                ? throw new EntityNotFoundException("The company with the supplied id was not found")
                : _mapper.Map<CompanyDto>(result);
        }
    }
}
