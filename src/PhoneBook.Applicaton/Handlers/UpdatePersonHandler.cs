﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Models;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record UpdatePersonRequest(UpdatePersonDto UpdatedPerson) : IRequest<bool>;

    public class UpdatePersonHandler : IRequestHandler<UpdatePersonRequest, bool>
    {
        private readonly IPersonRepository _personRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public UpdatePersonHandler(IPersonRepository personRepository, ICompanyRepository companyRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public async Task<bool> Handle(UpdatePersonRequest request, CancellationToken cancellationToken)
        {
            var currentPerson = await _personRepository.GetByIdAsync(request.UpdatedPerson.Id, cancellationToken) 
                ?? throw new EntityNotFoundException("The person with the supplied id was not found");

            var company = await _companyRepository.GetByNameAsync(request.UpdatedPerson.CompanyName, cancellationToken)
                ?? throw new ValidationException("The company name supplied does not exist");

            var updatedPerson = _mapper.Map<Person>(request.UpdatedPerson);
            updatedPerson.CompanyId = company.Id;
            updatedPerson.Company = null;

            await _personRepository.UpdatePersonAsync(currentPerson, updatedPerson, cancellationToken);
            return true;
        }
    }
}
