﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record SearchPeopleRequest(
            string? LastName = null,
            string? FirstName = null,
            string? AddressLine1 = null,
            string? AddressLine2 = null,
            string? Town = null,
            string? CompanyName = null) : IRequest<IReadOnlyCollection<PersonDto>>;

    public class SearchPeopleHandler : IRequestHandler<SearchPeopleRequest, IReadOnlyCollection<PersonDto>>
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public SearchPeopleHandler(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }

        public async Task<IReadOnlyCollection<PersonDto>> Handle(SearchPeopleRequest request, CancellationToken cancellationToken)
        {
            var people = await _personRepository.SearchAsync(request.LastName,
                request.FirstName, 
                request.AddressLine1,
                request.AddressLine2,
                request.Town,
                request.CompanyName, 
                cancellationToken);

            return _mapper.Map<IReadOnlyCollection<PersonDto>>(people);
        }
    }
}
