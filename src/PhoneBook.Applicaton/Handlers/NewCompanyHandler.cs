﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Models;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record NewCompanyRequest(NewCompnayDto NewCompnay) : IRequest<bool>;

    public class NewCompanyHandler : IRequestHandler<NewCompanyRequest, bool>
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public NewCompanyHandler(ICompanyRepository companyRepository, IMapper mapper)
        {
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public async Task<bool> Handle(NewCompanyRequest request, CancellationToken cancellationToken)
        {
            //check if same company name
            if (await _companyRepository.GetByNameAsync(request.NewCompnay.Name, cancellationToken) != null)
                throw new ValidationException("There is already another company with this name");

            await _companyRepository.AddAsync(_mapper.Map<Company>(request.NewCompnay), cancellationToken);
            return true;
        }
    }
}
