﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record GetPersonRequest(string Id) : IRequest<PersonDto>;
    public class GetPersonHandler : IRequestHandler<GetPersonRequest, PersonDto>
    {
        private readonly IPersonRepository _personRepository;
        private readonly IRandomNumberGenerator _randomNumberGenerator;
        private readonly IMapper _mapper;

        public GetPersonHandler(IPersonRepository personRepository, IRandomNumberGenerator randomNumberGenerator, IMapper mapper)
        {
            _personRepository = personRepository;
            _randomNumberGenerator = randomNumberGenerator;
            _mapper = mapper;
        }

        public async Task<PersonDto> Handle(GetPersonRequest request, CancellationToken cancellationToken)
        {
            if (request.Id == "*")
            {
                var count = await _personRepository.CountPeopleAsync(cancellationToken: cancellationToken);

                if (count == 0)
                    throw new ValidationException("There are no people stored");

                var randomIndex = _randomNumberGenerator.Next(count);
                return _mapper.Map<PersonDto>(await _personRepository.GetByNumberAsync(randomIndex, cancellationToken));
            }

            var success = uint.TryParse(request.Id, out var id);
            if (!success)
                throw new ValidationException("Invalid id supplied");

            var result = await _personRepository.GetByIdAsync(id, cancellationToken);

            return result == null
                ? throw new EntityNotFoundException("The person with the supplied id was not found")
                : _mapper.Map<PersonDto>(result);
        }
    }
}
