﻿using MediatR;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record DeletePersonRequest(uint Id) : IRequest<bool>;

    public class DeletePersonHandler : IRequestHandler<DeletePersonRequest, bool>
    {
        private readonly IPersonRepository _personRepository;

        public DeletePersonHandler(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<bool> Handle(DeletePersonRequest request, CancellationToken cancellationToken)
        {
            var currentPerson = await _personRepository.GetByIdAsync(request.Id, cancellationToken)
                ?? throw new EntityNotFoundException("The person with the supplied id was not found");

            await _personRepository.DeletePersonAsync(currentPerson, cancellationToken);
            return true;
        }
    }
}
