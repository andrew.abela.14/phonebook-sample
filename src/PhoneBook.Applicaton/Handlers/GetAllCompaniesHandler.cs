﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record GetAllCompaniesRequest() : IRequest<IReadOnlyCollection<CompanyDto>>;

    public class GetAllCompaniesHandler : IRequestHandler<GetAllCompaniesRequest, IReadOnlyCollection<CompanyDto>>
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly IPersonRepository _personRepository;

        public GetAllCompaniesHandler(ICompanyRepository companyRepository, IPersonRepository personRepository)
        {
            _companyRepository = companyRepository;
            _personRepository = personRepository;
        }

        public async Task<IReadOnlyCollection<CompanyDto>> Handle(GetAllCompaniesRequest request, CancellationToken cancellationToken)
        {
            var companies = await _companyRepository.GetAllAsync(cancellationToken);

            return await Task.WhenAll(companies.Select(async c => new CompanyDto
            {
                Name = c.Name,
                RegistrationDate = c.RegistrationDate,
                NoPeople = await _personRepository.CountPeopleAsync(c.Id, cancellationToken)
            }));
        }
    }
}
