﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.Applicaton.Exceptions;
using PhoneBook.DataAccess.Models;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record NewPersonRequest(PersonDto NewPerson) : IRequest<bool>;

    public class NewPersonHandler : IRequestHandler<NewPersonRequest, bool>
    {
        private readonly IPersonRepository _personRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IMapper _mapper;

        public NewPersonHandler(IPersonRepository personRepository, ICompanyRepository companyRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _companyRepository = companyRepository;
            _mapper = mapper;
        }

        public async Task<bool> Handle(NewPersonRequest request, CancellationToken cancellationToken)
        {
            var company = await _companyRepository.GetByNameAsync(request.NewPerson.CompanyName, cancellationToken) 
                ?? throw new ValidationException("The company name supplied does not exist");

            //set compnay id
            var person = _mapper.Map<Person>(request.NewPerson);
            person.CompanyId = company.Id;
            person.Company = null;

            await _personRepository.AddAsync(person, cancellationToken);
            return true;
        }
    }
}
