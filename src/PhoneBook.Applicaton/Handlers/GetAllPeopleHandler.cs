﻿using AutoMapper;
using MediatR;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.Applicaton.Handlers
{
    public record GetAllPeopleRequest() : IRequest<IReadOnlyCollection<PersonDto>>;

    public class GetAllPeopleHandler : IRequestHandler<GetAllPeopleRequest, IReadOnlyCollection<PersonDto>>
    {
        private readonly IPersonRepository _personRepository;
        private readonly IMapper _mapper;

        public GetAllPeopleHandler(IPersonRepository personRepository, IMapper mapper)
        {
            _personRepository = personRepository;
            _mapper = mapper;
        }

        public async Task<IReadOnlyCollection<PersonDto>> Handle(GetAllPeopleRequest request, CancellationToken cancellationToken)
        {
            var people = await _personRepository.GetAllAsync(cancellationToken);
            return _mapper.Map<IReadOnlyCollection<PersonDto>>(people);
        }
    }
}
