﻿namespace PhoneBook.Applicaton
{
    public interface IRandomNumberGenerator
    {
        int Next(int max);
    }
}
