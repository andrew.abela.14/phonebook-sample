﻿namespace PhoneBook.Applicaton
{
    public class RandomNumberGenerator : IRandomNumberGenerator
    {
        private readonly Random _rand;

        public RandomNumberGenerator()
            => _rand = new Random();

        public int Next(int max)
            => _rand.Next(max);
    }
}
