﻿using AutoMapper;
using PhoneBook.Applicaton.Dtos;
using PhoneBook.DataAccess.Models;

namespace PhoneBook.Applicaton.Convertors
{
    public class AutoMappingProfile : Profile
    {
        public AutoMappingProfile() 
        {
            CreateMap<NewCompnayDto, Company>();
            CreateMap<Company, CompanyDto>()
                .ForMember(dto => dto.NoPeople, options => options.MapFrom(c => c.People.Count));
            CreateMap<Person, PersonDto>()
                .ForMember(dto => dto.CompanyName, options => options.MapFrom(p => p.Company.Name))
                .ReverseMap()
                .ForPath(p => p.Company.Name, options => options.MapFrom(dto => dto.CompanyName));
            CreateMap<UpdatePersonDto, Person>()
                .ForPath(p => p.Company.Name, options => options.MapFrom(dto => dto.CompanyName));
        }
    }
}
