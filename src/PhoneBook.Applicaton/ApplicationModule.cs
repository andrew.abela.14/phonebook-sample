﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.Applicaton.Convertors;
using System.Reflection;

namespace PhoneBook.Applicaton
{
    public static class ApplicationModule
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddSingleton(_ => AddMappingProfile());
            services.AddSingleton<IRandomNumberGenerator, RandomNumberGenerator>();
            services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(ApplicationModule).Assembly));
            return services;
        }

        private static IMapper AddMappingProfile()
        {
            var mapperConfig = new MapperConfiguration(mc => { mc.AddProfile(new AutoMappingProfile()); });
            return mapperConfig.CreateMapper();
        }
    }
}