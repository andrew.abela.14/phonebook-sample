﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PhoneBook.DataAccess.Repositories;

namespace PhoneBook.DataAccess
{
    public static class DataAccesssModule
    {
        public static IServiceCollection AddDataAccess(this IServiceCollection services, IConfiguration configuration)
        {
            return services
                .AddDbContext<PhoneBookDbContext>(options => options.UseMySQL(configuration.GetConnectionString("PhoneBookDb")))
                .AddTransient<ICompanyRepository, CompanyRepository>()
                .AddTransient<IPersonRepository, PersonRepository>();
        }
    }
}