﻿using System;
using System.Collections.Generic;

namespace PhoneBook.DataAccess.Models
{
    public partial class Person
    {
        public uint Id { get; set; }
        public string LastName { get; set; } = null!;
        public string FirstName { get; set; } = null!;
        public string? AddressLine1 { get; set; }
        public string? AddressLine2 { get; set; }
        public string Town { get; set; } = null!;
        public uint CompanyId { get; set; }

        public virtual Company Company { get; set; } = null!;
    }
}
