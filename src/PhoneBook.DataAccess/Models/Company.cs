﻿using System;
using System.Collections.Generic;

namespace PhoneBook.DataAccess.Models
{
    public partial class Company
    {
        public Company()
        {
            People = new HashSet<Person>();
        }

        public uint Id { get; set; }
        public string Name { get; set; } = null!;
        public DateTime RegistrationDate { get; set; }

        public virtual ICollection<Person> People { get; set; }
    }
}
