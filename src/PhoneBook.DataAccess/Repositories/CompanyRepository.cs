﻿using Microsoft.EntityFrameworkCore;
using PhoneBook.DataAccess.Models;

namespace PhoneBook.DataAccess.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly PhoneBookDbContext _dbContext;

        public CompanyRepository(PhoneBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(Company newCompany, CancellationToken cancellationToken = default)
        {
            await _dbContext.Companies.AddAsync(newCompany, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<IReadOnlyCollection<Company>> GetAllAsync(CancellationToken cancellationToken = default)
            => await _dbContext.Companies.ToListAsync(cancellationToken);

        public async Task<Company?> GetByIdAsync(uint id, CancellationToken cancellationToken = default)
           => await _dbContext.Companies.Where(c => c.Id == id).Include(c => c.People).FirstOrDefaultAsync(cancellationToken);

        public async Task<Company?> GetByNameAsync(string name, CancellationToken cancellationToken = default)
           => await _dbContext.Companies.FirstOrDefaultAsync(c => c.Name.ToLower() == name.ToLower(), cancellationToken);
    }
}
