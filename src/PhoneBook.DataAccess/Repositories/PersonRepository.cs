﻿using Microsoft.EntityFrameworkCore;
using PhoneBook.DataAccess.Models;

namespace PhoneBook.DataAccess.Repositories
{
    public class PersonRepository : IPersonRepository
    {
        private readonly PhoneBookDbContext _dbContext;

        public PersonRepository(PhoneBookDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task AddAsync(Person newPerson, CancellationToken cancellationToken = default)
        {
            await _dbContext.People.AddAsync(newPerson, cancellationToken);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<IReadOnlyCollection<Person>> GetAllAsync(CancellationToken cancellationToken = default)
          => await _dbContext.People.Include(p => p.Company).ToListAsync(cancellationToken);

        public async Task<IReadOnlyCollection<Person>> SearchAsync(
            string? lastName,
            string? firstName,
            string? addressLine1,
            string? addressLine2,
            string? town,
            string? companyName,
            CancellationToken cancellationToken = default)
            => await _dbContext.People.Where(p => (lastName == null || p.LastName == lastName) && (firstName == null || p.FirstName == lastName)
                 && (addressLine1 == null || p.AddressLine1 == addressLine1) && (addressLine2 == null || p.AddressLine2 == addressLine2)
                 && (town == null || p.Town == town))
                .Include(c => c.Company)
                .Where(p => companyName == null || p.Company.Name == companyName)
                .ToListAsync(cancellationToken);

        public async Task<Person?> GetByIdAsync(uint id, CancellationToken cancellationToken = default)
           => await _dbContext.People.Where(p => p.Id == id).Include(p => p.Company).FirstOrDefaultAsync(cancellationToken);

        public async Task<Person> GetByNumberAsync(int number, CancellationToken cancellationToken = default)
            => await _dbContext.People.Skip(number).FirstAsync(cancellationToken);

        public async Task UpdatePersonAsync(Person person, Person updatedPerson, CancellationToken cancellationToken = default)
        {
            _dbContext.Entry(person).CurrentValues.SetValues(updatedPerson);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<int> CountPeopleAsync(uint? companyId = null, CancellationToken cancellationToken = default)
        {
            if (companyId is null)
                return await _dbContext.People.CountAsync(cancellationToken);

            return await _dbContext.People.Where(p => p.CompanyId == companyId.Value).CountAsync(cancellationToken);
        }

        public async Task DeletePersonAsync(Person person, CancellationToken cancellationToken = default)
        {
            _dbContext.Remove(person);
            await _dbContext.SaveChangesAsync(cancellationToken);
        }
    }
}
