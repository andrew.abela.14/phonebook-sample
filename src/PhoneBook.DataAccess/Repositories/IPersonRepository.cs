﻿using PhoneBook.DataAccess.Models;

namespace PhoneBook.DataAccess.Repositories
{
    public interface IPersonRepository
    {
        Task AddAsync(Person newPerson, CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<Person>> GetAllAsync(CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<Person>> SearchAsync(
            string? lastName,
            string? firstName,
            string? addressLine1,
            string? addressLine2,
            string? town,
            string? companyName,
            CancellationToken cancellationToken = default);
        Task<Person?> GetByIdAsync(uint id, CancellationToken cancellationToken = default);
        Task<Person> GetByNumberAsync(int number, CancellationToken cancellationToken = default);
        Task<int> CountPeopleAsync(uint? companyId = null, CancellationToken cancellationToken = default);
        Task UpdatePersonAsync(Person person, Person updatedPerson, CancellationToken cancellationToken = default);
        Task DeletePersonAsync(Person person, CancellationToken cancellationToken = default);
    }
}
