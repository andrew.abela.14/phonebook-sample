﻿using PhoneBook.DataAccess.Models;

namespace PhoneBook.DataAccess.Repositories
{
    public interface ICompanyRepository
    {
        Task AddAsync(Company newCompany, CancellationToken cancellationToken = default);

        Task<IReadOnlyCollection<Company>> GetAllAsync(CancellationToken cancellationToken = default);

        Task<Company?> GetByIdAsync(uint id, CancellationToken cancellationToken = default);

        Task<Company?> GetByNameAsync(string name, CancellationToken cancellationToken = default);
    }
}
