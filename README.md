# phonebook-sample

## Getting started

MYSQL DB = phonebookdb
Uid = admin
Password = admin1234!

schema in "/schema/tables_creation.sql"

db first approach -> Scaffold-DbContext "server=127.0.0.1;database=phonebookdb;uid=admin;pwd=admin1234!" MySql.EntityFrameworkCore -context PhoneBookDbContext -ContextDir . -OutputDir Models

MediatR -> mediator pattern

Automapper -> to convert request/dtos

Testing -> xUnit

Microsoft.EntityFrameworkCore.InMemory -> for an in memory db

AutoFixture -> random generate data

FluentAssertions -> for test assertions

Moq -> for mocking interfaces



